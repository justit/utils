export * as format from './format.js' // 格式化工具
export * as test from './test.js' // 校验工具
export { default as scrollTo } from './scroll-to.js' // 滚动条
export * from './tool.js' // 常用工具函数
