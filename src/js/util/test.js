// @ts-check
// 校验工具

/**
 * 验证电子邮箱格式
 * @param {String} value - 处理的值
 * @return {Boolean} 是否为邮箱
 */
export const email = (value) => {
  return /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(value);
}

/**
 * 验证手机格式
 * @param {Number|String} value - 处理的值
 * @return {Boolean} 是否为手机
 */
export const mobile = (value) => {
  let str = String(value)
  return /^1[23456789]\d{9}$/.test(str)
}

/**
 * 验证身份证号码
 * @param {Number|String} value - 处理的值
 * @return {Boolean} 是否为身份证号码
 */
export const idCard = (value) => {
  let str = String(value)
  return /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(str)
}

/**
 * 金额,只允许2位小数
 * @param {Number|String} value - 处理的值
 * @return {Boolean} 是否为只有2位小数的值
 */
export const amount = (value) => {
  let str = String(value)
  //金额，只允许保留两位小数
  return /^[1-9]\d*(,\d{3})*(\.\d{1,2})?$|^0\.\d{1,2}$/.test(str);
}

/**
 * 只能是字母或者数字
 * @param {Number|String} value - 处理的值
 * @return {Boolean} 是否为字母或者数字
 */
export const enOrNum = (value) => {
  let str = String(value)
  return /^[0-9a-zA-Z]*$/g.test(str);
}

/**
 * 只能是数字或小数
 * @param {Number|String} value - 处理的值
 * @return {Boolean} 是否为数字或小数
 */
export const floatOrNum = (value) => {
  let str = String(value)
  return /^\d+(\.\d+)?$/.test(str);
}

/**
 * 验证一个值范围
 * @param {Number|String} value - 处理的值
 * @param {[Number, Number]} param - 范围数组 [min, max]
 * @return {Boolean} 值的大小是否在范围内
 * 
 * @example
 * range(10, [0, 20])
 */
export const range = (value, param) => {
  const num = Number(value);
  return num >= param[0] && num <= param[1]
}

/**
 * 验证一个长度范围
 * @param {Number|String} value - 处理的值
 * @param {[Number, Number]} param - 范围数组 [min, max]
 * @return {Boolean} 值的长度是否在范围内
 * 
 * @example
 * rangeLength(111, [0, 5])
 */
export const rangeLength = (value, param) => {
  let str = String(value)
  return str.length >= param[0] && str.length <= param[1]
}

/**
 * 严格的对象类型检查。
 * @param {Object} obj - 传入对象
 * @return {Boolean} 仅对纯JavaScript对象返回true
 */
export const isPlainObject = (obj) => {
  const _toString = Object.prototype.toString;
  return _toString.call(obj) === '[object Object]'
}