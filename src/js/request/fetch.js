// 一个 Promise 就是一个代表了异步操作最终完成或者失败的对象
const DOMAIN = ''

/**
 * 验证URL格式
 */
function url(value) {
    return /http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w-.\/?%&=]*)?/.test(value)
}

/**
 * 合并路径
 * @param {String} value - 传入的路径后缀
 */
function mergeUrl(value) {
    return url(value) ? value : (DOMAIN + (value.indexOf('/') == 0 ? value : '/' + value))
}

/**
 * 对象转换url参数
 * @param {Object} data - 传入的参数对象
 * @returns {String} - 转换后的参数字符串
 */
function queryParams(data = {}) {
    let prefix = '?'
    let _result = []
    for (let key in data) {
        let value = data[key]
        // 去掉为空的参数
        if (['', undefined, null].indexOf(value) >= 0) {
            continue;
        }
        _result.push(key + '=' + value)
    }
    return _result.length ? prefix + _result.join('&') : ''
}

export default class http {
    static get = (url, data) => {

        let urls = mergeUrl(url)
        if (data) {
            urls = urls + queryParams(data)
        }
        return fetch(urls) // 默认是GET
            .then(response => response.json()) // 把数据解析成json格式,然后取出
    };
    static post = (url, data) => {
        let otherUrls = ['/exportasdmy'] // 导出

        return fetch(mergeUrl(url), {
            method: 'POST',
            headers: {
                'Accept': 'application/json', // 告诉服务器，我们能接受json格式的返回类型，
                'Content-Type': 'application/json;charset=UTF-8', //告诉服务器，我们提交的数据类型
            },
            body: JSON.stringify(data), // (把你想提交得数据序列化为json字符串类型，然后提交)body中的数据就是我们需要向服务器提交的数据,比如用户名,密码等
        }) // 返回 服务器处理的结果
            .then(response => {
                if (!otherUrls.includes(url)) { // 对导出文件路径判断
                    return response.json()
                } else {
                    return response.blob()
                }
            })
    }
}