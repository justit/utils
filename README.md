# utils

#### 介绍
常用工具仓库

#### 软件架构

ts类型补充
- 放在vue src文件夹下，对自行封装方法类型补充
---
js

- request、fetch
```
import http from 'request.js'

http.post(url, data).then()
```

- format
```
import { format, scrollTo, test, <tool>} from 'util' // 'util/index.js'

// format 格式化工具

// scrollTo 控制页面滚动条

// test 校验工具

// tool 工具函数 包含 debounce, timeDifference, awaitTo, AOP等

```

- scroll-to
```
import scrollTo from './scroll-to.js'

scrollTo(0, 800) // 滚动条移动（位置， 时间）
```
---
css
```
// 重置页面默认样式
<link rel="stylesheet" href="./css/reset.css">
// flex样式封装
<link rel="stylesheet" href="./css/common.css">
```